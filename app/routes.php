<?php

// Controllers

use App\Controllers\BackOfficeController;
use App\Controllers\FrontOfficeController;
use App\Controllers\UserController;

/////////////////////////////////////////////////////////////

// Authentification
$app->get('/login', UserController::class . ':login_page')->setName('login');
$app->post('/login_post', UserController::class. ':login');

$app->get('/logout', UserController::class. ':logout')->setName('logout');


// FrontOffice
$app->get('/', FrontOfficeController::class. ':home_page')->setName('home');


// BackOffice
$app->group('/admin', function(){
    $this->get('', BackOfficeController::class . ':admin_page')->setName('admin');
    $this->get('/create_account', BackOfficeController::class . ':create_account_page')->setName('create_account');
});
