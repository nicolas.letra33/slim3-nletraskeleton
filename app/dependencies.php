<?php

use Aptoma\Twig\Extension\MarkdownExtension;
use Aptoma\Twig\Extension\MarkdownEngine;

$container = $app->getContainer();

// Twig
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__.'/../views', [
        'cache' => false
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container->get('request')->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container->get('router'), $basePath));
    // markdown extension
    $engine = new MarkdownEngine\MichelfMarkdownEngine();
    $view->addExtension(new MarkdownExtension($engine));

    return $view;
};

// ----------------------
// Models instanciations
// ----------------------

// FrontOffice Model
$container['frontoffice_model'] = function($app) {
    $db = $app['settings']['db'];
    return new App\Models\FrontOfficeModel($db['hostname'], $db['database'], $db['user'], $db['password']);
};

// BackOffice Model
$container['backoffice_model'] = function($app) {
    $db = $app['settings']['db'];
    return new App\Models\BackOfficeModel($db['hostname'], $db['database'], $db['user'], $db['password']);
};

// user Model
$container['user_model'] = function($app) {
    $db = $app['settings']['db'];
    return new App\Models\UserModel($db['hostname'], $db['database'], $db['user'], $db['password']);
};