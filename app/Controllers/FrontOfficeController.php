<?php

namespace App\Controllers;

/**
 * Contrôleur de la partie frontoffice de l'application
 */
class FrontOfficeController extends Controller
{
    /**
     * Retourne la page d'accueil de l'application
     * @param $request : requête http
     * @param $response : reponse http
     * @param array $args : tableau d'arguments
     */
    public function home_page($request, $response, array $args){
        return $this->c->view->render($response,'frontoffice/home.html.twig');
    }
}