<?php

namespace App\Controllers;

/**
 * Contrôleur de la partie backoffice de l'application
 */
class BackOfficeController extends Controller
{
    /**
     * Retourne la page d'administration
     * @param $request : requête http
     * @param $response : reponse http
     * @param array $args : tableau d'arguments
     */
    public function admin_page($request, $response, array $args){
        return $this->adminAccess($response,'backoffice/admin.html.twig');
    }

    /**
     * Retourne la page de création de compte
     * @param $request : requête http
     * @param $response : reponse http
     * @param array $args : tableau d'arguments
     */
    public function create_account_page($request, $response, array $args){
        return $this->adminAccess($response,'backoffice/create_account.html.twig');
    }
}