<?php

namespace App\Controllers;

/**
 * Contrôleur de la gestion d'utilisateur et de connexion
 */
class UserController extends Controller
{
    /**
     * Retourne la page de login
     * @param $request : requête http
     * @param $response : reponse http
     * @param array $args : tableau d'arguments
     */
    public function login_page($request, $response, array $args){
        return $this->c->view->render($response,'login.html.twig');
    }

    /**
     * Vérifie si les champs rentrés dans le formulaire d'authentification correspondent à un compte en base
     * Si c'est le cas, une variable de session est initialisée
     * Sinon l'application redirige vers la page de login
     * @param $request : requête http
     * @param $response : reponse http
     * @param array $args : tableau d'arguments
     */
    public function login($request,$response,array $args){

        $user = $this->c->user_model->getByUser($_POST["user_name"]);

        if(password_verify($_POST["user_password"], $user["password"])){
            $_SESSION['auth'] = 1;
            return $response->withRedirect($this->c->router->pathFor('admin'));
        }
        return $response->withRedirect($this->c->router->pathFor('login'));
    }

    /**
     * Déconnexion de l'utilisateur par une suppression de la session courante
     * Redirige ensuite vers la page d'accueil de l'application
     * @param $request : requête http
     * @param $response : reponse http
     * @param array $args : tableau d'arguments
     */
    public function logout($request, $response, array $args){
        session_destroy();
        return $response->withRedirect($this->c->router->pathFor('home'));
    }
}