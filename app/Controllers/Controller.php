<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;

/**
 * Classe contrôleur dont hériteront tous les contrôleurs de l'application.
 */
abstract class Controller
{
    protected $c;

    /**
     * Constructeur d'un contrôleur
     * Initialise la session si il n'y a pas de session existante
     * Défini un conteneur qui permettra de préparer, gérer et d'injecter les dépendances du projet
     * @param ContainerInterface $c : conteneur Slim
     */
    public function __construct(ContainerInterface $c){
        if(session_status() === PHP_SESSION_NONE){
            session_start();
        }
        
        $this->c = $c;
    }

    /**
     * Vérifie si l'utilisateur est authentifié
     * @param ResponseInterface $response : Réponse http
     */
    protected function isAuth($response){
        if(isset($_SESSION['auth']) && $_SESSION['auth'] === 1){
            return true;
        }
        else{
            return false;
        }
    }

    /** 
     * Permet à l'utilisateur d'accéder à la page voulu s'il est authentifié, sinon redirige sur la page de login
     * @param ResponseInterface $response : Réponse http
     * @param string $path : chemin de la vue à retourner en cas de vérification de la condition
     */
    protected function adminAccess($response, string $path){
        if($this->isAuth($response)){
            return $this->c->view->render($response, $path);
        }
        else{
            return $response->withRedirect($this->c->router->pathFor('login'));
        }
    }
    
}