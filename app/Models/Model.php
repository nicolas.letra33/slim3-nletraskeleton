<?php

namespace App\Models;

/**
 * Classe modèle dont hériteront tous les modèles de l'application.
 */
class Model
{
    protected $pdo;

    /**
     * Constructeur d'un modèle
     * Se connecte à la base de données via PDO
     * @param string $host : IP du serveur mysql
     * @param string $database : nom de la base de données
     * @param string $user : user de la base de données
     * @param string $password : mot de passe du user
     */
    public function __construct(string $host, string $database, string $user, string $password)
    {
        try {
            $this->pdo = new \PDO(
                'mysql:dbname='.$database.';host='.$host,
                $user,
                $password
            );
        } catch (\PDOException $error) {
            die('Unable to connect to database.');
        }
        $this->pdo->exec('SET CHARSET UTF8');
    }

    /**
     * Permet d'exécuter des requêtes sql
     * @param \PDOStatement $query : requête sql
     * @param array $var
     */
    protected function execute(\PDOStatement $query, array $var = [])
    {
        if (!$query->execute($var)) {
            $errors = $query->errorInfo();
            throw new ModelException($errors[2]);
        }

        return $query;
    }

    /**
     * Permet de retourner un seul résultat pour une requête donnée
     * @param \PDOStatement $query : requête sql
     */
    protected function fetchOne(\PDOStatement $query)
    {
        if ($query->rowCount() != 1) {
            return false;
        } else {
            return $query->fetch();
        }
    }
}