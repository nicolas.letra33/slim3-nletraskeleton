<?php

namespace App\Models;

/**
 * Modèle de la gestion d'utilisateur
 */
class UserModel extends Model
{
    /**
     * Requête SQL permettant d'obtenir les données relatives des utilisateurs
     */
    public function getByUserSQL(){
        return 'SELECT *
                FROM users ';
    }

    /**
     * Retourne les informations d'un utilisateur passé en paramètre
     * @param string $username : nom de l'utilisateur
     */
    public function getByUser(string $username){
        $sql = $this->getByUserSQL() . 'WHERE user = ?';
        $query = $this->pdo->prepare($sql);
        $this->execute($query, [$username]);
        return $this->fetchOne($query);
    }
}