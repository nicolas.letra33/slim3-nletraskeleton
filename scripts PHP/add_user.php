<?php

$db = [
    'hostname' => 'localhost',
    'user' => 'root',
    'password' => '',
    'database' => 'demo',
];

$pdo = new \PDO('mysql:dbname='.$db['database'].';host='.$db['hostname'],$db['user'],$db['password']);


$user = "admin";
$hash = password_hash("toto", PASSWORD_DEFAULT);

$status = $pdo->exec(
    "INSERT INTO users (user, password) VALUES ('{$user}', '{$hash}')"
);