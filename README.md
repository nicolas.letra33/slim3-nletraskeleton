# Slim3-nLetraSkeleton

## I - Présentation
Ce projet est un squelette de site web qui utilise le micro-framework **Slim php** en version 3.  
Le framework Slim, par sa nature de micro-framework permet une structure libre de son squelette d'application.  
Ce projet est une possibilité de structure, d'autres sont possibles, ici ce sont un ensemble de choix personnels que j'ai fait lors de la réalisation du squelette.  

Pour présenter la structure de l'application, j'ai ajouté à ce projet une branche "démo" à titre d'exemple de la façon dont s'utilise Slim3 et ce squelette en particulier.  
Le projet présente également un système d'authentification avec une page admin protégée par mot de passe.

## II - Prérequis

PHP  
Composer  
Une base de donnée 

## III - Lancement du projet

La première étape est d'installer les dépendances du projet, pour celà on utilise composer et la commande suivante à la racine du projet :  
`composer install`  

Ensuite, dans sa base de donnée local, lancer les requêtes sql présentes dans le dossier **scripts SQL** afin de créer la base de données utilisée pour la démo.  
Par défaut, l'application utilise une base de donnée mysql avec un user "root" et sans mot de passe. Il est possible de changer cette configuration via le fichier settings.php et Model.php qui seront détaillés dans la prochaine partie.  

Pour insérer un utilisateur dans la base de données, executer **add_user.php** présent dans le dossier **scripts PHP**.  

Une fois la base de donnée en place, on peut lancer le serveur de developpement au niveau du dossier public via la commande suivante :  
`cd public`  
`php -S localhost:8000`  

Le site de démo est désormais accessible à l'adresse : **http://localhost:8000/** 

## IV - Structure du projet

Dans cette section je vais détailler chaque partie de ce squelette d'application Slim 3.

Tout d'abord, ces deux fichiers à la racine :

- **composer.json** : Il s'agit des dépendances du projet qui sont gérées et installées par composer.

- **scripts SQL** : Contient les requêtes SQL a utiliser pour créer la base de donnée, la table utilisateur et les tables utilisées par la branche de demo.  

- **scripts PHP** : Contient le script PHP à utiliser pour ajouter un utilisateur dans la base, par défaut le username est "admin" et le mot de passe "toto".  

Ensuite le coeur de l'application et ce qu'on va manipuler est réparti dans trois dossiers :

- **public** : Le coeur de l'application, composé de :
	
	+ **index.php** : fichier permettant de lancer l'application après avoir démarré une session, charger les paramètres, charger les dépendances et charger le script gérant les routes.
	
	+ **un dossier css** : Pour contenir les différents fichiers css d'un projet.
	
	+ **un dossier img** : Pour contenir les différentes images d'un projet.
	
- **app** : Contient la partie back-end de l'application, composé de  :

	+ **Un dossier Controller** : Contient les controllers de l'application et présente déjà une classe controller de laquelle les controleurs qu'on crée pourront hériter. (la classe Controller contient un attribut de type ContainerInterface utilisé par Slim pour faire intéragir les vues, controllers et models)
	
	+ **Un dosser Model** : Contient les models de l'application et présente déjà une classe model de laquelle les models qu'on crée pourront hériter. (la classe Model instancien une connexion pdo avec la base de donnée et contient une méthode permettant de ne renvoyer qu'un seul résultat)
	
	+ **dependencies.php** : Permet de gérer les dépendances du projet comme Twig ou les model en les ajoutant dans le container Slim.

	+ **routes.php** : Gère les routes de l'application. Ce projet contient trois exemples de route, une simple qui associe du html à une route, une classique qui appelle des méthodes du controller et une qui permet de grouper des routes partageant une partie de leur url commune.
	
	+ **settings.php** : Les paramètres de l'application, de slim, de la base de donnée ou encore de Twig.
	
- **views** : Contient la partie front-end de l'application avec les différentes vues et layout.

## V - Auteur

Nicolas Letra de Medeiros - Developpeur