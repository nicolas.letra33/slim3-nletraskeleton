CREATE TABLE users(
	id INT auto_increment NOT NULL,
	CONSTRAINT user PRIMARY KEY (id),
	user varchar(50) DEFAULT NULL,
	password text DEFAULT NULL
);