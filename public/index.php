<?php

$loader = include(__DIR__ . '/../vendor/autoload.php');
$loader->add('', __DIR__.'/../app/');

// Configuration selon l'environnement
require __DIR__ . '/../app/config.php';

// Instantiate the app
$settings = require __DIR__ . '/../app/settings.php';
$app = new \Slim\App($settings);

// Dependencies
require __DIR__ . '/../app/dependencies.php';

// Routes
require __DIR__ . '/../app/routes.php';

$app->run();
